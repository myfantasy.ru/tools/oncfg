package oncfg

import "encoding/json"

// AddIfNotExistsAsDict - joins main and add as main is primary value add updates ierarchy when value in main is not exists
// any error returns main
func AddIfNotExistsAsDict(main json.RawMessage, add json.RawMessage) json.RawMessage {
	var mMap map[string]json.RawMessage
	var aMap map[string]json.RawMessage
	err := json.Unmarshal(main, &mMap)
	if err != nil {
		return main
	}

	err = json.Unmarshal(add, &aMap)
	if err != nil {
		return main
	}

	for k, vA := range aMap {
		vM, ok := mMap[k]

		if ok {
			mMap[k] = AddIfNotExistsAsDict(vM, vA)
		} else {
			mMap[k] = vA
		}
	}

	res, err := json.Marshal(mMap)
	if err != nil {
		return main
	}

	return res
}
