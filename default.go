package oncfg

import (
	"reflect"
	"strconv"
	"time"
)

const DefaultTag = "default"
const DefaultPwdTag = "default_pwd"

type DefaultFromTag interface {
	DefaultFromTag(tag string)
}

func SetDefault(s interface{}) {
	SetDefaultTag(s, DefaultTag, true)
}

func SetDefaultPwd(s interface{}) {
	SetDefaultTag(s, DefaultPwdTag, true)
}

var durationType = reflect.TypeOf(time.Second)

func SetDefaultTag(s interface{}, tagName string, createPointers bool) {
	value := reflect.ValueOf(s).Elem()

	setDefaultTag(value, tagName, createPointers, "")
}

func SetDefaultTagToType(value reflect.Value, defaults string) bool {
	if defaults == "" {
		return false
	}

	if value.Kind() == reflect.Pointer {
		if !value.IsNil() {
			SetDefaultTagToType(value.Elem(), defaults)
		}
	}

	if value.CanAddr() {
		if dft, ok := value.Addr().Interface().(DefaultFromTag); ok {
			dft.DefaultFromTag(defaults)
			return true
		}
	}

	if value.Type() == (durationType) {
		if value.Int() == 0 {
			if durValue, err := time.ParseDuration(defaults); err == nil {
				value.SetInt(int64(durValue))
			}
		}
		return true
	}

	if defaults != "" && value.IsZero() {
		switch value.Kind() {
		case reflect.String:
			value.SetString(defaults)
			return true
		case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
			if value.Int() == 0 {
				if intValue, err := strconv.ParseInt(defaults, 10, 64); err == nil {
					value.SetInt(intValue)
				}
			}
			return true
		case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64:
			if value.Uint() == 0 {
				if uintValue, err := strconv.ParseUint(defaults, 10, 64); err == nil {
					value.SetUint(uintValue)
				}
			}
			return true
		case reflect.Float32, reflect.Float64:
			if value.Float() == 0 {
				if floatValue, err := strconv.ParseFloat(defaults, 64); err == nil {
					value.SetFloat(floatValue)
				}
			}
			return true
		case reflect.Bool:
			if !value.Bool() {
				if boolValue, err := strconv.ParseBool(defaults); err == nil {
					value.SetBool(boolValue)
				}
			}
			return true
		}
	}

	return false
}

func setDefaultTag(value reflect.Value, tagName string, createPointers bool, tagValue string) {
	typ := value.Type()

	for i := 0; i < typ.NumField(); i++ {
		field := value.Field(i)
		defaults := typ.Field(i).Tag.Get(tagName)

		if field.Kind() == reflect.Struct || field.Kind() == reflect.Interface {
			if SetDefaultTagToType(field, defaults) {
				continue
			}
		}

		if field.Kind() == reflect.Struct {
			setDefaultTag(field, tagName, createPointers, defaults)
			continue
		}

		if field.Kind() == reflect.Pointer && createPointers {
			if !field.CanSet() {
				continue
			}
			if field.IsNil() {
				val := reflect.New(field.Type().Elem())
				field.Set(val)

				setDefaultTag(val.Elem(), tagName, createPointers, defaults)
			}

			continue
		}

		if field.Kind() == reflect.Pointer {
			if !field.IsNil() {
				setDefaultTag(field.Elem(), tagName, createPointers, defaults)
			}
		}

		SetDefaultTagToType(field, defaults)
	}
}
