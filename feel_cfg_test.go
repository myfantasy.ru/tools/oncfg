package oncfg

import (
	"testing"
	"time"

	"github.com/myfantasy/mfctx/jsonify"
)

func TestFeelCfg(t *testing.T) {
	m := map[string]string{
		"baSe_conn_string":             "user=postgres host=localhost port=5432 dbname=postgres",
		"baSe_maX_connections":         "5",
		"baSe_open_timeout":            "600s",
		"baSe_iNn_cfg_conn_string":     "user=postgres host=localhost port=5432 dbname=postgres",
		"BASE_inn_cfg_max_connections": "5",
		"baSe_inn_cfg_open_timeout":    "600s",
		"baSe_EX2_open_timeout":        "600s",
	}

	ex := TestDefaultStruct{}
	FeelConfigWide(&ex, m, "_", "envconfig", "base", false, false)

	if ex.ConnString == "" {
		t.Errorf("conn string should be not empty %v", jsonify.JsonifySLn(ex))
	}

	if ex.Password != "" {
		t.Errorf("password string should be empty %v", jsonify.JsonifySLn(ex))
	}

	if ex.OpenTimeout != 600*time.Second {
		t.Errorf("OpenTimeout time.Duration should be empty %v", jsonify.JsonifySLn(ex))
	}

	if ex.InnerDefaultStruct.OpenTimeout != 600*time.Second {
		t.Errorf("InnerDefaultStruct.OpenTimeout time.Duration should be empty %v", jsonify.JsonifySLn(ex))
	}

	if ex.Ex2 == nil {
		t.Errorf("Ex2 should be not empty %v", jsonify.JsonifySLn(ex))
	}

	if ex.Ex2 != nil && ex.Ex2.OpenTimeout != 600*time.Second {
		t.Errorf("Ex2.OpenTimeout time.Duration be not empty %v", jsonify.JsonifySLn(ex))
	}
}
