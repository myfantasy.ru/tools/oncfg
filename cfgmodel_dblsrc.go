package oncfg

import (
	"context"
	"sync"
)

type DoubleSourceConfig struct {
	fisrt  ConfigRaw
	second ConfigRaw

	isFirstSet  bool
	isSecondSet bool

	summ      ConfigRaw
	isSummSet bool

	nextFeelCfg FeelCfgFunc

	mx sync.Mutex
}

func DoubleSourceConfigCreate(nextFeelCfg FeelCfgFunc) *DoubleSourceConfig {
	return &DoubleSourceConfig{
		nextFeelCfg: nextFeelCfg,
	}
}

func (dsc *DoubleSourceConfig) FeelFirstCfg(ctx context.Context, cfgRaw *ConfigRaw) (err error) {
	dsc.mx.Lock()
	defer dsc.mx.Unlock()

	dsc.fisrt = *cfgRaw
	dsc.isFirstSet = true

	return dsc.agg(ctx)
}

func (dsc *DoubleSourceConfig) FeelSecondCfg(ctx context.Context, cfgRaw *ConfigRaw) (err error) {
	dsc.mx.Lock()
	defer dsc.mx.Unlock()

	dsc.second = *cfgRaw
	dsc.isSecondSet = true

	return dsc.agg(ctx)
}

func (dsc *DoubleSourceConfig) agg(ctx context.Context) (err error) {
	if dsc.isFirstSet && dsc.isSecondSet {
		dsc.summ, err = ConfigRawDictUinion(dsc.fisrt, dsc.second)
		if err != nil {
			return err
		}
		dsc.isSummSet = true

		if dsc.nextFeelCfg != nil {
			err = dsc.nextFeelCfg(ctx, &dsc.summ)
			if err != nil {
				return err
			}
		}
	}

	return nil
}

func (dsc *DoubleSourceConfig) Get() (cfg ConfigRaw, ok bool) {
	dsc.mx.Lock()
	defer dsc.mx.Unlock()

	return dsc.summ, dsc.isSummSet
}
