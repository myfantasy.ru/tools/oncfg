package oncfg

import (
	"encoding/json"
	"fmt"
	"reflect"
)

var ErrParsibleUnmarshalBodyLen = fmt.Errorf("body should be not emptu")

type Parsible[T any] struct {
	Value     T
	IsNotNil  bool
	IsParsed  bool
	IsDefault bool
}

var _ json.Marshaler = Parsible[struct{}]{}
var _ json.Unmarshaler = &Parsible[struct{}]{}

func (p Parsible[T]) MarshalJSON() ([]byte, error) {
	if !p.IsNotNil {
		return []byte("null"), nil
	}
	return json.Marshal(p.Value)
}

func (p *Parsible[T]) UnmarshalJSON(body []byte) error {
	p.IsParsed = true
	if len(body) == 0 {
		return ErrParsibleUnmarshalBodyLen
	}

	if body[0] == 'n' {
		return nil
	}

	p.IsNotNil = true

	return json.Unmarshal(body, &p.Value)
}

func (p *Parsible[T]) DefaultFromTag(tag string) {
	p.IsDefault = true
	p.IsNotNil = true
	SetDefaultTagToType(reflect.ValueOf(&p.Value), tag)
}
