package oncfg

import (
	"context"
	"encoding/json"
	"fmt"
	"sync"
)

var ErrListSourceConfigNotReady = fmt.Errorf("not ready")
var ErrListSourceConfigNotFound = fmt.Errorf("not found")

type CallAfterUpdateFunc func(ctx context.Context)

type ListSourceConfig struct {
	base ConfigRaw

	isSplitted bool
	keys       []string
	values     map[string]ConfigRaw
	refreshers map[string]FeelCfgFunc

	callAfterUpdateFunc CallAfterUpdateFunc

	mx sync.Mutex
}

func ListSourceConfigCreate() *ListSourceConfig {
	return &ListSourceConfig{
		refreshers: make(map[string]FeelCfgFunc),
	}
}

func (lsc *ListSourceConfig) Keys(ctx context.Context) ([]string, error) {
	lsc.mx.Lock()
	defer lsc.mx.Unlock()

	if !lsc.isSplitted {
		return nil, ErrListSourceConfigNotReady
	}

	return lsc.keys, nil
}

func (lsc *ListSourceConfig) Config(ctx context.Context, name string) (cfg ConfigRaw, err error) {
	lsc.mx.Lock()
	defer lsc.mx.Unlock()

	if !lsc.isSplitted {
		return cfg, ErrListSourceConfigNotReady
	}

	cfg, ok := lsc.values[name]
	if !ok {
		return cfg, ErrListSourceConfigNotFound
	}

	return cfg, nil
}

func (lsc *ListSourceConfig) FeelCfg(ctx context.Context, cfgRaw *ConfigRaw) (err error) {
	lsc.mx.Lock()
	defer lsc.mx.Unlock()

	lsc.base = *cfgRaw

	jsObj := map[string]json.RawMessage{}

	err = json.Unmarshal(lsc.base.Cfg, &jsObj)
	if err != nil {
		return err
	}

	lsc.isSplitted = true

	keys := []string{}
	values := map[string]ConfigRaw{}
	refreshers := map[string]FeelCfgFunc{}

	for k, v := range jsObj {
		keys = append(keys, k)
		cfgOne := ConfigRaw{
			DataCenter:  cfgRaw.DataCenter,
			AppName:     cfgRaw.AppName,
			Name:        cfgRaw.Name + "->" + k,
			Cfg:         v,
			Version:     cfgRaw.Version,
			Description: cfgRaw.Description,
		}
		values[k] = cfgOne
		refreshers[k] = lsc.refreshers[k]

		if refreshers[k] != nil {
			go refreshers[k](ctx, &cfgOne)
		}
	}

	lsc.keys = keys
	lsc.values = values
	lsc.refreshers = refreshers

	if lsc.callAfterUpdateFunc != nil {
		go lsc.callAfterUpdateFunc(ctx)
	}

	return nil
}

func (lsc *ListSourceConfig) SetCallAfterUpdateFunc(callAfterUpdateFunc CallAfterUpdateFunc) {
	lsc.mx.Lock()
	defer lsc.mx.Unlock()

	lsc.callAfterUpdateFunc = callAfterUpdateFunc
}

func (lsc *ListSourceConfig) SetFeelFunc(name string, feelFunc FeelCfgFunc) {
	lsc.mx.Lock()
	defer lsc.mx.Unlock()

	lsc.refreshers[name] = feelFunc
}
