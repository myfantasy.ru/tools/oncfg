package oncfg

import (
	"testing"
	"time"

	"github.com/myfantasy/mfctx/jsonify"
)

type Ex2 struct {
	OpenTimeout time.Duration `envconfig:"open_timeout" default:"600s" json:"open_timeout"`
}

type TestDefaultStruct struct {
	ConnString         string        `envconfig:"conn_string" default:"user=postgres host=localhost port=5432 dbname=postgres" json:"conn_string"`
	Password           string        `envconfig:"password" default_pwd:"postgres" json:"password"`
	MaxConnections     int           `envconfig:"max_connections" default:"5" json:"max_connections"`
	OpenTimeout        time.Duration `envconfig:"open_timeout" default:"600s" json:"open_timeout"`
	InnerDefaultStruct struct {
		ConnString     string        `envconfig:"conn_string" default:"user=postgres host=localhost port=5432 dbname=postgres" json:"conn_string"`
		Password       string        `envconfig:"password" default_pwd:"postgres" json:"password"`
		MaxConnections int           `envconfig:"max_connections" default:"5" json:"max_connections"`
		OpenTimeout    time.Duration `envconfig:"open_timeout" default:"600s" json:"open_timeout"`
	} `envconfig:"inn_cfg"`
	Ex2 *Ex2
}

func TestDefault(t *testing.T) {
	ex := TestDefaultStruct{}
	SetDefault(&ex)

	if ex.ConnString == "" {
		t.Errorf("conn string should be not empty %v", jsonify.JsonifySLn(ex))
	}

	if ex.Password != "" {
		t.Errorf("password string should be empty %v", jsonify.JsonifySLn(ex))
	}

	if ex.OpenTimeout != 600*time.Second {
		t.Errorf("OpenTimeout time.Duration should be empty %v", jsonify.JsonifySLn(ex))
	}

	if ex.InnerDefaultStruct.OpenTimeout != 600*time.Second {
		t.Errorf("InnerDefaultStruct.OpenTimeout time.Duration should be empty %v", jsonify.JsonifySLn(ex))
	}

	if ex.Ex2 == nil {
		t.Errorf("Ex2 should be not empty %v", jsonify.JsonifySLn(ex))
	}

	if ex.Ex2 != nil && ex.Ex2.OpenTimeout != 600*time.Second {
		t.Errorf("Ex2.OpenTimeout time.Duration be not empty %v", jsonify.JsonifySLn(ex))
	}
}
