package oncfg

import (
	"encoding/json"
	"testing"
)

func TestAddIfNotExistsAsDict(t *testing.T) {
	main := `{
	"a":5,
	"c":10,
	"d":{
		"a":5,
		"c":6,
		"d":{
		}
	}
}`
	add := `{
	"b":5,
	"c":15,
	"d":{
		"b":5,
		"c":10,
		"e":{
		}
	}
}`

	res := AddIfNotExistsAsDict(json.RawMessage(main), json.RawMessage(add))

	if string(res) != `{"a":5,"b":5,"c":10,"d":{"a":5,"b":5,"c":6,"d":{},"e":{}}}` {
		t.Error(string(res))
	}
}
