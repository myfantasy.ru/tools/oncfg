module gitlab.com/myfantasy.ru/tools/oncfg

go 1.21.0

require github.com/myfantasy/mfctx v1.0.1

require (
	github.com/go-logr/logr v1.4.1 // indirect
	github.com/go-logr/stdr v1.2.2 // indirect
	github.com/myfantasy/ints v1.0.1 // indirect
	go.opentelemetry.io/otel v1.25.0 // indirect
	go.opentelemetry.io/otel/metric v1.25.0 // indirect
	go.opentelemetry.io/otel/trace v1.25.0 // indirect
)
