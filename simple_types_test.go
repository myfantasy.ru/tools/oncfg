package oncfg

import (
	"encoding/json"
	"testing"

	"github.com/myfantasy/mfctx/jsonify"
)

func TestSimpleTypes(t *testing.T) {
	js := `{
		"ab"   :    null,
		"cd"   :    6
	}`

	type TTP struct {
		Ab Parsible[int] `json:"ab"`
		Cd Parsible[int] `json:"cd"`
	}

	var ttp TTP

	err := json.Unmarshal([]byte(js), &ttp)

	if err != nil {
		t.Error(err)
	}
	if !ttp.Ab.IsParsed {
		t.Error("shouls be parsed")
	}
	if ttp.Ab.IsNotNil {
		t.Error("shouls be nil")
	}
	if !ttp.Cd.IsNotNil {
		t.Error("shouls be not nil")
	}
	if ttp.Cd.Value != 6 {
		t.Errorf("should be 6 but %v", jsonify.JsonifySLn(ttp))
	}
	// t.Error(jsonify.JsonifySLn(ttp))
}

func TestSimpleTypesDefault(t *testing.T) {
	type TTP struct {
		Ab Parsible[int] `default:"600" json:"ab"`
		Cd Parsible[int] `default:"7600" json:"cd"`
	}

	var ttp TTP
	SetDefault(&ttp)

	res := `{"Ab": {"Value": 600,"IsNotNil": true,"IsParsed": false,"IsDefault": true},"Cd": {"Value": 7600,"IsNotNil": true,"IsParsed": false,"IsDefault": true}}`

	if jsonify.JsonifySM(ttp) != res {
		t.Error(jsonify.JsonifySM(ttp))
	}
}
