package oncfg

import (
	"reflect"
)

func GrabConfigWide(v any, params map[string]string, separator string, tag string, prefix string) {

	grabConfigWide(reflect.ValueOf(v).Elem(), params, separator, tag, prefix)
}

func grabConfigWide(value reflect.Value, params map[string]string, separator string, tag string, prefix string) {
	typ := value.Type()

	for i := 0; i < typ.NumField(); i++ {
		field := value.Field(i)
		paramName := typ.Field(i).Tag.Get(tag)

		if paramName == "" {
			continue
		}

		fieldName := prefix + separator + paramName

		if field.Kind() == reflect.Struct || field.Kind() == reflect.Interface {
			if GrabTagFromType(field, paramName, params) {
				continue
			}
		}

		if field.Kind() == reflect.Struct {
			grabConfigWide(field, params, separator, tag, fieldName)
			continue
		}

		if field.Kind() == reflect.Pointer {
			if field.IsNil() {
				val := reflect.New(field.Type().Elem())
				field.Set(val)

				grabConfigWide(val.Elem(), params, separator, tag, fieldName)
			}

			continue
		}

		if field.Kind() == reflect.Pointer {
			if !field.IsNil() {
				grabConfigWide(field.Elem(), params, separator, tag, fieldName)
			}
		}

		GrabTagFromType(field, paramName, params)
	}
}

type ValueForTag interface {
	ValueForTag() (value string)
}

func GrabTagFromType(value reflect.Value, paramName string, params map[string]string) bool {
	if paramName == "" {
		return false
	}

	if value.Kind() == reflect.Pointer {
		if !value.IsNil() {
			GrabTagFromType(value.Elem(), paramName, params)
		}
	}

	if value.CanAddr() {
		if dft, ok := value.Addr().Interface().(ValueForTag); ok {
			v := dft.ValueForTag()
			params[paramName] = v
			return true
		}
	}

	switch value.Kind() {
	case reflect.String:
		params[paramName] = value.String()
		return true
	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
		params[paramName] = value.String()
		return true
	case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64:
		params[paramName] = value.String()
		return true
	case reflect.Float32, reflect.Float64:
		params[paramName] = value.String()
		return true
	case reflect.Bool:
		params[paramName] = value.String()
		return true
	}

	return false
}
