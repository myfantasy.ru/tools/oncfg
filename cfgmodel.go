package oncfg

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	"github.com/myfantasy/mfctx"
)

var ErrNotFoundConfig = fmt.Errorf("config not found")
var ErrUnionConfigListEmptyFail = fmt.Errorf("config list should be not nil")

const SettingsNameLogParam = "settings_name"
const HasDiffLogParam = "has_diff"

type FeelCfgFunc func(ctx context.Context, cfgRaw *ConfigRaw) (err error)
type GetTimeoutFunc func() time.Duration

type ConfigRaw struct {
	DataCenter  string          `json:"data_center"`
	AppName     string          `json:"app_name"`
	Name        string          `json:"name"`
	Cfg         json.RawMessage `json:"cfg"`
	Version     string          `json:"version"`
	Description string          `json:"description"`
}

func ConfigRawDictUinion(cfgs ...ConfigRaw) (cfgRes ConfigRaw, err error) {
	if len(cfgs) == 0 {
		return cfgRes, ErrUnionConfigListEmptyFail
	}

	if len(cfgs) == 1 {
		return cfgs[0], nil
	}

	cfg := cfgs[0]

	dataCenter := cfg.DataCenter
	appName := cfg.AppName
	name := cfg.Name
	cfgVal := cfg.Cfg
	version := cfg.Version
	descr := cfg.Description

	for i, c := range cfgs {
		if i == 0 {
			continue
		}
		version += "; " + c.DataCenter + ":" + c.AppName + ":" + c.Name + "|" + c.Version
		cfgVal = AddIfNotExistsAsDict(cfgVal, c.Cfg)
		descr += "\n\n" + c.Name + ":" + c.Description
	}

	if err != nil {
		return cfgRes, err
	}

	cfgRes.DataCenter = dataCenter
	cfgRes.AppName = appName
	cfgRes.Name = name
	cfgRes.Version = version
	cfgRes.Description = descr
	cfgRes.Cfg = cfgVal

	return cfgRes, nil
}

type ConfigSource interface {
	GetConfig(ctx context.Context, name string) (cfgRaw *ConfigRaw, exists bool, err error)
}

func LoadAndFeel(ctx context.Context, sc ConfigSource, name string,
	prevCfgRaw *ConfigRaw, feelCfg FeelCfgFunc,
) (cfgRaw *ConfigRaw, ok bool, err error) {
	cr, exists, err := sc.GetConfig(ctx, name)
	if err != nil {
		return nil, false, err
	}

	if !exists {
		return nil, false, fmt.Errorf("config name:%v error: %w", name, ErrNotFoundConfig)
	}

	if prevCfgRaw != nil && prevCfgRaw.Version == cr.Version {
		return cr, false, nil
	}

	err = feelCfg(ctx, cr)
	if err != nil {
		return cr, true, err
	}

	return cr, true, nil
}

// LoadPermonent - load cfg every timeout; run LoadPermonent in go
func LoadPermonent(ctxIn context.Context, sc ConfigSource, name string, timeout GetTimeoutFunc, feelCfg FeelCfgFunc) {
	ctx := mfctx.FromCtx(ctxIn).Start("oncfg.LoadPermonent")
	ctx.With(SettingsNameLogParam, name)
	defer func() { ctx.Complete(ctxIn.Err()) }()

	var cfgRaw *ConfigRaw
Loop:
	for ctx.Err() == nil {
		func() {
			ctx := ctx.Start("oncfg.LoadPermonent.func")
			var err error
			var ok bool
			var cr *ConfigRaw
			defer func() { ctx.Complete(err) }()
			cr, ok, err = LoadAndFeel(ctx, sc, name, cfgRaw, feelCfg)
			ctx.With(HasDiffLogParam, ok)
			if err != nil {
				return
			}
			if ok {
				cfgRaw = cr
			}
		}()
		select {
		case <-time.After(timeout()):
		case <-ctx.Done():
			break Loop
		}
	}
}
