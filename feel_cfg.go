package oncfg

import (
	"reflect"
	"strings"
)

func FeelConfigWide(v any, params map[string]string, separator string, tag string, prefix string, sensitive bool, notCreatePointers bool) {
	if !sensitive {
		paramsUnSens := make(map[string]string, len(params))

		for k, v := range params {
			paramsUnSens[strings.ToLower(k)] = v
		}

		params = paramsUnSens
	}

	feelConfigWide(reflect.ValueOf(v).Elem(), params, separator, tag, prefix, sensitive, notCreatePointers)
}
func feelConfigWide(value reflect.Value, params map[string]string, separator string, tag string, prefix string, sensitive bool, notCreatePointers bool) {
	typ := value.Type()

	for i := 0; i < typ.NumField(); i++ {
		field := value.Field(i)
		f := typ.Field(i)
		paramName := typ.Field(i).Tag.Get(tag)

		if paramName == "" {
			paramName = f.Name
		}

		fieldName := prefix + separator + paramName

		if !sensitive {
			fieldName = strings.ToLower(fieldName)
		}

		defaults := params[fieldName]

		if field.Kind() == reflect.Struct || field.Kind() == reflect.Interface {
			if SetDefaultTagToType(field, defaults) {
				continue
			}
		}

		if field.Kind() == reflect.Struct {
			feelConfigWide(field, params, separator, tag, fieldName, sensitive, notCreatePointers)
			continue
		}

		if field.Kind() == reflect.Pointer && !notCreatePointers {
			if !field.CanSet() {
				continue
			}
			if field.IsNil() {
				val := reflect.New(field.Type().Elem())
				field.Set(val)

				feelConfigWide(val.Elem(), params, separator, tag, fieldName, sensitive, notCreatePointers)
			}

			continue
		}

		if field.Kind() == reflect.Pointer {
			if !field.IsNil() {
				feelConfigWide(field.Elem(), params, separator, tag, fieldName, sensitive, notCreatePointers)
			}
		}

		SetDefaultTagToType(field, defaults)
	}
}
